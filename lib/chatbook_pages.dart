import 'package:flutter/material.dart';

class ChakbookPages extends StatefulWidget {
  const ChakbookPages({Key? key}) : super(key: key);

  @override
  State<ChakbookPages> createState() => _ChakbookPagesState();
}

class _ChakbookPagesState extends State<ChakbookPages> {
  TextEditingController messagerEditCont=TextEditingController();
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        actions: [Icon(Icons.close)],
      ),
      body: SafeArea(
        child: Padding(
          padding: const EdgeInsets.all(10.0),
          child: Container(
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.stretch,
              children: [
                Container(
                  child: Row(
                    children: [
                      Expanded(
                        child: TextField(
                          controller: messagerEditCont,
                          decoration: InputDecoration(
                              contentPadding: EdgeInsets.symmetric(
                                  vertical: 4, horizontal: 20),
                              fillColor: Colors.green,
                              filled: true,
                              hintText: "Type your massage here",
                              hintStyle: TextStyle(color: Colors.white70)),
                        ),
                      ),
                      TextButton(
                        onPressed: () {},
                        child: Text("Send"),
                      )
                    ],
                  ),
                )
              ],
            ),
          ),
        ),
      ),
    );
  }
}
