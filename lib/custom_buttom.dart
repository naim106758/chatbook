import 'package:flutter/material.dart';
class CustomButton extends StatelessWidget {

  final String? text;
  final Function()? onPressed;
  const CustomButton({Key? key,this.text,this.onPressed}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return MaterialButton(
      height: 20,
      minWidth:300,
      color: Colors.cyan,
      hoverColor: Colors.greenAccent,
      onPressed:onPressed,
      shape: StadiumBorder(),
      child: Padding(
        padding: EdgeInsets.only(top: 12, bottom: 12, right: 16, left: 16),
        child: Text(
          text!,
          style: TextStyle(
            color: Colors.white,fontSize: 17,fontWeight: FontWeight.normal
           ),
          textAlign: TextAlign.center,
        ),
      ),
    );
  }
}
