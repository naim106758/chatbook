import 'package:flutter/material.dart';
import 'package:messenger/regiter_pages.dart';

import 'custom_buttom.dart';
import 'login.dart';
class HomePages extends StatefulWidget {
  const HomePages({Key? key}) : super(key: key);

  @override
  State<HomePages> createState() => _HomePagesState();
}

class _HomePagesState extends State<HomePages> with SingleTickerProviderStateMixin{
  AnimationController? animationController;
  Animation? animation;

  @override
  void initState() {
    // TODO: implement initState
    animationController = AnimationController(
      vsync:this,
      duration: Duration(seconds:7),
    );
    animation = ColorTween(begin: Colors.white, end: Colors.blue.shade200)
        .animate(animationController!);
    animationController!.forward();
    animationController!.addListener(() {
      setState(() {});
      print(animationController!.value);
    });
  }
  @override
  void dispose() {
    super.dispose();
    animationController!.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: animation!.value, //Colors.white,
      body: SafeArea(
        child: Padding(
          padding: const EdgeInsets.all(8.0),
          child: Container(
            padding: const EdgeInsets.symmetric(),
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              //crossAxisAlignment: CrossAxisAlignment.center,
              children: [
                Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    Hero(
                      tag: "logo_tag",
                      child: SizedBox(
                        height:40,
                        child:Image.asset("assets/images/logo.png"),
                      ),
                    ),
                    Text("ChatBook",style: TextStyle(fontSize:30,fontWeight: FontWeight.bold),)
                  ],
                ),
                CustomButton(
                  text: "Login",
                  onPressed: (){
                    Navigator.push(context, MaterialPageRoute(builder: (context)=>Login()));
                  },
                ),
                SizedBox(height: 20,),
                CustomButton(
                  text: "Register",
                  onPressed: (){
                    Navigator.push(context, MaterialPageRoute(builder: (context)=>RegiterPages()));
                  },
                ),

              ],
            ),
          ),
        ),
      ),
    );
  }
}
