import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:messenger/custom_buttom.dart';
import 'package:rflutter_alert/rflutter_alert.dart';
import 'chatbook_pages.dart';

class Login extends StatefulWidget {
  const Login({Key? key}) : super(key: key);

  @override
  State<Login> createState() => _LoginState();
}

class _LoginState extends State<Login> {
  bool isVisible = true;
TextEditingController MailEditingController=TextEditingController();
TextEditingController PasswordEditingController=TextEditingController();
FirebaseAuth? firebaseAuth;
  bool? showSpinner;

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    firebaseAuth = FirebaseAuth?.instance;
    showSpinner = false;
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        //leading: Icon(Icons.add),
      ),
      body: SafeArea(
        child: Container(
          padding: const EdgeInsets.only(left: 20, right: 20),
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
                  Hero(
                    tag: "logo_tag",
                    child: SizedBox(
                      height:40,
                      child:Image.asset("assets/images/logo.png"),
                    ),
                  ),
              TextField(
                onChanged: (value){
                  setState(() {
                    
                  });
                },
                controller: MailEditingController,
                 obscureText: false,
                decoration: InputDecoration(
                  hintText: "Enter Your Mail",
                  labelText: "Mail",
                  prefixIcon: Icon(Icons.person),
                  suffixIcon:MailEditingController.text.isEmpty?Text(" "): GestureDetector(
                    onTap: (){
                      MailEditingController.clear();
                    },
                    child: Icon(Icons.clear),),
                  border: OutlineInputBorder(
                      borderRadius: BorderRadius.all(Radius.circular(12))),
                ),
              ),
              SizedBox(
                height: 20,
              ),
              TextField(
                controller: PasswordEditingController,
                 obscureText: isVisible,
                decoration: InputDecoration(
                  hintText: "Enter password",
                  labelText: "password",
                  prefixIcon: Icon(Icons.lock),
                  suffixIcon:GestureDetector(
                    onTap: (){
                      isVisible=!isVisible;
                      setState(() {

                      });
                    },
                    child: Icon(Icons.visibility),),
                  border: OutlineInputBorder(borderRadius: BorderRadius.all(Radius.circular(12))),
                ),
              ),
              SizedBox(height: 20,),
              CustomButton(
                text: "Login",
                onPressed: () async {
                  if (MailEditingController.text.isEmpty) {
                    Alert(
                        context: context,
                        type: AlertType.error,
                        title: "mail",
                        desc: "Please Enter your mail address")
                        .show();
                  } else if (PasswordEditingController.text.length < 5) {
                    Alert(
                        context: context,
                        type: AlertType.error,
                        title: "mail",
                        desc: "Please Enter your mail address")
                        .show();
                  } else {
                    setState(() {
                      showSpinner=true;
                    });
                    try{
                      UserCredential userCard = await firebaseAuth!
                          .signInWithEmailAndPassword(
                          email: MailEditingController!.text,
                          password: PasswordEditingController!.text);
                      if (userCard.user!.email == MailEditingController.text) {
                        Navigator.push(context,
                            MaterialPageRoute(builder: (context) => ChakbookPages()));
                      } else {
                        setState(() {
                          showSpinner=false;
                        });
                        Alert(
                            context: context,
                            type: AlertType.error,
                            title: "Failed LogIn",
                            desc: " ")
                            .show();
                      }
                    }catch(error){
                      setState(() {
                        showSpinner=false;
                      });
                      Alert(
                          context: context,
                          type: AlertType.error,
                          title: "Failed LogIn",
                          desc:error.toString())
                          .show();
                    }

                  }
                  // try{
                  //   await firebaseAuth!.createUserWithEmailAndPassword(
                  //       email: MailEditingController.text,
                  //       password: PasswordEditingController.text);
                  //   Navigator.push(context, MaterialPageRoute(builder: (context)=>Login()));
                  // }catch(error){
                  //   print(error);
                  // }
                },
              )
            ],
          ),
        ),
      ),
    );
  }
}
