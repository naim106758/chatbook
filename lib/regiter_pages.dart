import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:messenger/home_Pages.dart';
import 'package:messenger/login.dart';
import 'package:modal_progress_hud_nsn/modal_progress_hud_nsn.dart';
import 'package:rflutter_alert/rflutter_alert.dart';
import 'custom_buttom.dart';
class RegiterPages extends StatefulWidget {
  const RegiterPages({Key? key}) : super(key: key);

  @override
  State<RegiterPages> createState() => _RegiterPagesState();
}

class _RegiterPagesState extends State<RegiterPages> {
  bool isVisible = true;
  TextEditingController FristNameEditingController = TextEditingController();
  TextEditingController LastNameEditingController = TextEditingController();
  TextEditingController MailEditingController = TextEditingController();
  TextEditingController NumberEditingController = TextEditingController();
  TextEditingController PasswordEditingController = TextEditingController();
  FirebaseAuth? firebaseAuth;
  bool? showSpinner;

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    firebaseAuth = FirebaseAuth?.instance;
    showSpinner = false;
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(),
      body: SafeArea(
        child: ModalProgressHUD(
          inAsyncCall: showSpinner!,
          child: Container(
            padding: const EdgeInsets.only(left: 20, right: 20),
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                Hero(
                  tag: "logo_tag",
                  child: SizedBox(
                    height: 40,
                    child: Image.asset("assets/images/logo.png"),
                  ),
                ),
                //Name
                TextField(
                  onChanged: (value) {
                    setState(() {});
                  },
                  controller: FristNameEditingController,
                  obscureText: false,
                  decoration: InputDecoration(
                      hintText: "Enter Your FristName",
                      labelText: "Name",
                      prefixIcon: Icon(Icons.person),
                      suffixIcon: FristNameEditingController.text.isEmpty
                          ? Text(" ")
                          : GestureDetector(
                              onTap: () {
                                FristNameEditingController.clear();
                              },
                              child: Icon(Icons.close),
                            ),
                      border: OutlineInputBorder(
                          borderRadius: BorderRadius.circular(15))),
                ),
                SizedBox(
                  height: 15,
                ),
                TextField(
                  onChanged: (value) {
                    setState(() {});
                  },
                  controller: LastNameEditingController,
                  obscureText: false,
                  decoration: InputDecoration(
                      hintText: "Enter Your LastName",
                      labelText: "Name",
                      prefixIcon: Icon(Icons.person),
                      suffixIcon: LastNameEditingController.text.isEmpty
                          ? Text(" ")
                          : GestureDetector(
                              onTap: () {
                                LastNameEditingController.clear();
                              },
                              child: Icon(Icons.close),
                            ),
                      border: OutlineInputBorder(
                          borderRadius: BorderRadius.circular(15))),
                ),
                SizedBox(
                  height: 15,
                ),
                //mail
                TextField(
                  onChanged: (value) {
                    setState(() {});
                  },
                  controller: MailEditingController,
                  obscureText: false,
                  decoration: InputDecoration(
                      hintText: "Enter Your Mail",
                      labelText: "Mail",
                      prefixIcon: Icon(Icons.mail),
                      suffixIcon: MailEditingController.text.isEmpty
                          ? Text(" ")
                          : GestureDetector(
                              onTap: () {
                                MailEditingController.clear();
                              },
                              child: Icon(Icons.close),
                            ),
                      border: OutlineInputBorder(
                          borderRadius: BorderRadius.circular(12))),
                ),
                SizedBox(
                  height: 20,
                ),
                //Number
                TextField(
                  onChanged: (value) {
                    setState(() {});
                  },
                  controller: NumberEditingController,
                  obscureText: false,
                  decoration: InputDecoration(
                      hintText: "Enter Your Phone Numaber",
                      labelText: "Number",
                      prefixIcon: Icon(Icons.phone),
                      suffixIcon: NumberEditingController.text.isEmpty
                          ? Text(" ")
                          : GestureDetector(
                              onTap: () {
                                NumberEditingController.clear();
                              },
                              child: Icon(Icons.close),
                            ),
                      border: OutlineInputBorder(
                          borderRadius: BorderRadius.circular(12))),
                ),
                SizedBox(
                  height: 20,
                ),
                //Password
                TextField(
                  cursorWidth:1,
                  controller: PasswordEditingController,
                  obscureText: isVisible,
                  decoration: InputDecoration(
                      hintText: "Enter Your Password",
                      labelText: "Password",
                      prefixIcon: Icon(Icons.lock),
                      suffixIcon: GestureDetector(
                        onTap: () {
                          isVisible = !isVisible;
                          setState(() {});
                        },
                        child: Icon(Icons.visibility),
                      ),
                      border: OutlineInputBorder(
                          borderRadius: BorderRadius.circular(12))),
                ),
                SizedBox(
                  height: 20,
                ),
                CustomButton(
                  text: "Register",
                  onPressed: () async {
                    if (MailEditingController.text.isEmpty) {
                      Alert(
                              context: context,
                              type: AlertType.error,
                              title: "mail",
                              desc: "Please Enter your mail address")
                          .show();
                    } else if (PasswordEditingController.text.length < 5) {
                      Alert(
                              context: context,
                              type: AlertType.error,
                              title: "mail",
                              desc: "Please Enter your mail address")
                          .show();
                    } else {
                      setState(() {
                        showSpinner=true;
                      });
                      try{
                        UserCredential userCard = await firebaseAuth!
                            .createUserWithEmailAndPassword(
                            email: MailEditingController!.text,
                            password: PasswordEditingController!.text);
                        if (userCard.user!.email == MailEditingController.text) {
                          Navigator.push(context,
                              MaterialPageRoute(builder: (context) => Login()));
                        } else {
                          setState(() {
                            showSpinner=false;
                          });
                          Alert(
                              context: context,
                              type: AlertType.error,
                              title: "Failed Reg",
                              desc: " ")
                              .show();
                        }
                      }catch(error){
                        setState(() {
                          showSpinner=false;
                        });
                        Alert(
                            context: context,
                            type: AlertType.error,
                            title: "Failed Reg",
                            desc:error.toString())
                            .show();
                      }

                    }
                    // try{
                    //   await firebaseAuth!.createUserWithEmailAndPassword(
                    //       email: MailEditingController.text,
                    //       password: PasswordEditingController.text);
                    //   Navigator.push(context, MaterialPageRoute(builder: (context)=>Login()));
                    // }catch(error){
                    //   print(error);
                    // }
                  },
                )
              ],
            ),
          ),
        ),
      ),
    );
  }
}
